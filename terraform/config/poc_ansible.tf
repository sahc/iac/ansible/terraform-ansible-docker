resource "azurerm_virtual_machine" "poc_vm" {
  name                  = "VM-POC-${var.main_software}"
  location              = "${var.location}"
  resource_group_name   = "${azurerm_resource_group.poc_rg.name}"
  network_interface_ids = ["${azurerm_network_interface.public_vm_nic.id}"]
  vm_size               = "Standard_DS1_v2"

  #This will delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  tags {
    os = "${var.image_offer}"
    main-software = "${var.main_software}"
  }

  storage_image_reference {
    publisher = "${var.image_publisher}"
    offer     = "${var.image_offer}"
    sku       = "${var.image_sku}"
    version   = "${var.image_version}"
  }

  storage_os_disk {
    name          = "osdisk-poc-${var.main_software}"
    vhd_uri       = "${azurerm_storage_account.poc_storage.primary_blob_endpoint}${azurerm_storage_container.poc_container.name}/osdisk-poc-${var.main_software}.vhd"
    caching       = "ReadWrite"
    create_option = "FromImage"
  }

  os_profile {
    computer_name  = "poc${var.main_software}"
    admin_username = "${var.vm_username}"
    admin_password = "${var.vm_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_network_security_group" "nsg_poc" {
  name 			= "NSG-POC-${var.main_software}"
  location 		= "${var.location}"
  resource_group_name 	= "${azurerm_resource_group.poc_rg.name}"

  security_rule {
    name 			= "AllowSSH"
    priority 		= 100
    direction 		= "Inbound"
    access 		        = "Allow"
    protocol 		= "Tcp"
    source_port_range       = "*"
    destination_port_range     	= "22"
    source_address_prefix      	= "*"
    destination_address_prefix 	= "*"
  }

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_subnet" "poc_subnet" {
  name 			= "POC-${var.main_software}-Subnet"
  address_prefix 	= "${var.subnet_cidr}"
  virtual_network_name 	= "${azurerm_virtual_network.poc_vnet.name}"
  resource_group_name 	= "${azurerm_resource_group.poc_rg.name}"
}

resource "azurerm_public_ip" "poc_pip" {
  name 				= "POC-${var.main_software}-PIP"
  location 			= "${var.location}"
  resource_group_name 		= "${azurerm_resource_group.poc_rg.name}"
  public_ip_address_allocation 	= "dynamic"
  domain_name_label = "${var.domain}"

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_network_interface" "public_vm_nic" {
  name 		      = "POC-${var.main_software}-NIC"
  location 	      = "${var.location}"
  resource_group_name = "${azurerm_resource_group.poc_rg.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg_poc.id}"

  ip_configuration {
    name 			= "Poc-${var.main_software}-Private"
    subnet_id 			= "${azurerm_subnet.poc_subnet.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id	= "${azurerm_public_ip.poc_pip.id}"
  }
  tags {
    group = "${var.group_name}"
  }
}
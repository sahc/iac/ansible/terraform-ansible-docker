resource "azurerm_resource_group" "poc_rg" {
  name 		= "${var.resource_group_name}"
  location 	= "${var.location}"

  tags {
    responsable = "Sergio Hume"
    cliente     = "Operacion TI"
    proyecto    = "POC-Terraform-Ansible-Docker"
  }
}

resource "azurerm_virtual_network" "poc_vnet" {
  name 			= "POC-Ansible-VNet"
  address_space 	= ["${var.vnet_cidr}"]
  location 		= "${var.location}"
  resource_group_name   = "${azurerm_resource_group.poc_rg.name}"

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_storage_account" "poc_storage" {
  name 			= "pocansiblestorage"
  resource_group_name 	= "${azurerm_resource_group.poc_rg.name}"
  location 		= "${var.location}"
  account_tier = "Standard"
  account_replication_type = "LRS"

  tags {
    group = "${var.group_name}"
  }

}

resource "azurerm_storage_container" "poc_container" {
  name 			= "pocansiblecontainer"
  resource_group_name 	= "${azurerm_resource_group.poc_rg.name}"
  storage_account_name 	= "${azurerm_storage_account.poc_storage.name}"
  container_access_type = "private"
}